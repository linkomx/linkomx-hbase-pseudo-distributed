# Linko MX - HBase Pseudo Distributed

Docker image of the HBase in Pseudo Distributed mode.

## Requirements

  - Docker 20

## Contributing

### Git config

```bash
$ git config --local "user.name" "myusernameatgitlab"
```

```bash
$ git config --local "user.email" "myemail@linko.mx"
```

## Building

### Local installation

For local installation use:

```bash
$ docker run \
    -u $(id -u):$(grep -w docker /etc/group | awk -F\: '{print $3}') \
    --rm \
    -w $(pwd) \
    -v /etc/group:/etc/group:ro \
    -v /etc/passwd:/etc/passwd:ro \
    -v $(pwd):$(pwd) \
    -v ${HOME}/.m2:${HOME}/.m2 \
    -v /var/run/docker.sock:/var/run/docker.sock \
    azul/zulu-openjdk-alpine:8u282 \
    ./mvnw -Djansi.force=true -ntp -U clean package
```

Then launch the HBase in _pseudo distributed mode_ with:

```bash
$ docker run -d \
    --net=host \
    --rm \
    --name=zookeeper \
    zookeeper:3.5.6
```

```bash
$ docker run -d \
    --net=host \
    --rm \
    --name=hbase \
    --add-host=zk-1:127.0.0.1 \
    --add-host=zk-2:127.0.0.1 \
    --add-host=zk-3:127.0.0.1 \
    --add-host=$(hostname):127.0.0.1 \
    linkomx-hbase-pseudo-distributed:2.2.7
```

